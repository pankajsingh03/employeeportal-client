package com.nagarro.employeeportalclient.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nagarro.employeeportalclient.dao.UserDao;
import com.nagarro.employeeportalclient.model.User;

@Service
public class UserService {

	@Autowired
	private UserDao userDao;

	@Transactional
	public int createUser(User user) {
		return this.userDao.saveUser(user);
	}

	@Transactional
	public User getUser(String username) {
		return this.userDao.getUser(username);
	}

}
