package com.nagarro.employeeportalclient.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nagarro.employeeportalclient.model.User;
import com.nagarro.employeeportalclient.service.UserService;

@Controller
public class VerifyLoginController {

	@Autowired
	private UserService userService;

	@RequestMapping(path = "/verifyLogin", method = RequestMethod.POST)
	public String verifyLogin(@RequestParam("username") String username, @RequestParam("password") String password,
			Model model) {

		System.out.println(username);
		System.out.println(password);

		String e = "#";

		User u = this.userService.getUser(username);

		if (u != null && u.getPassword().equals(password)) {
			model.addAttribute("username", username);
			model.addAttribute("errorId", e);
			System.out.println("password Matched Successfully");
			return "home";
		}
		e = "1";
		model.addAttribute("errorId", e);
		System.out.println("password Matching error");
		return "login";
	}

}
