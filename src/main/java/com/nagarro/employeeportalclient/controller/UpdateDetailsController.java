package com.nagarro.employeeportalclient.controller;

import org.springframework.expression.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.nagarro.employeeportalclient.model.Employee;

@Controller
public class UpdateDetailsController {
	
	@RequestMapping("/updateDetails")
	public String updateDetails(@RequestParam("employeeCode") int employeeCode,
			@RequestParam("employeeName") String employeeName, @RequestParam("location") String location,
			@RequestParam("email") String email, @RequestParam("dateOfBirth") String dateOfBirth,@RequestParam("username") String username, Model model)
			throws ParseException, java.text.ParseException 
	{  

		model.addAttribute("username", username);
		
		Employee employee=new Employee(employeeCode,employeeName,location,email,dateOfBirth);
		RestClient.updateEmployeeAPI(employeeCode,employee);
		
		
		
		return "home";
	}
	
}
