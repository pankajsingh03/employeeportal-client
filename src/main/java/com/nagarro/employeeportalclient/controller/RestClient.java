package com.nagarro.employeeportalclient.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.nagarro.employeeportalclient.model.Employee;

public class RestClient {

	private static final String GET_ALL_EMPLOYEE_API="http://localhost:8086/api/employees";
	private static final String GET_EMPLOYEE_BY_ID_API="http://localhost:8086/api/employees/{id}";
	private static final String CREATE_EMPLOYEE_API="http://localhost:8086/api/employees";
	private static final String UPDATE_EMPLOYEE_API="http://localhost:8086/api/employees/{id}";
	private static final String DELETE_EMPLOYEE_API="http://localhost:8086/api/employees/{id}";



	 static RestTemplate restTemplate = new RestTemplate();
	
	public static void main(String[] args)
	{
		
	}
	
	public static Employee[] getAllEmployeeAPI()
	{
		HttpHeaders headers= new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<Employee> entity= new HttpEntity<Employee>(headers);
		ResponseEntity<Employee[]> response = restTemplate.exchange(GET_ALL_EMPLOYEE_API, HttpMethod.GET,entity,Employee[].class);
		Employee[] employees = response.getBody();
	
		return employees;
	}
	
	
	public static Employee getEmployeeByIdAPI(int eCode)
	{   Map<String ,Integer> param =new HashMap<String, Integer>();
		param.put("id",eCode);
		Employee employee= restTemplate.getForObject(GET_EMPLOYEE_BY_ID_API, Employee.class,param);
		return employee ;
	}
	
	public static void creteEmployeeAPI(Employee employee)
	{ 
		ResponseEntity<Employee> e1= restTemplate.postForEntity(CREATE_EMPLOYEE_API, employee,Employee.class);
		
	}
	
	public static void updateEmployeeAPI(int eCode,Employee employee)
	{ 
		Map<String ,Integer> param =new HashMap<String, Integer>();
		param.put("id",eCode);
		restTemplate.put(UPDATE_EMPLOYEE_API, employee ,param);
		
	}
	
	public static void deleteAPI(int eCode)
	{ 
		
		Map<String ,Integer> param =new HashMap<String, Integer>();
		param.put("id",eCode);
		restTemplate.delete(DELETE_EMPLOYEE_API, param);
		
	}
	
}
