package com.nagarro.employeeportalclient.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

	@RequestMapping("/home")
	public String home() {
		return "home";
	}

	@RequestMapping("/")
	public String index() {

		return login();
	}

	@RequestMapping("/login")
	public String login() {

		return "login";
	}

	@RequestMapping("/edit")
	public String edit(@RequestParam("idToUpdate") int idToUpdate, @RequestParam("username") String username,
			Model model) {

		model.addAttribute("idToUpdate", idToUpdate);
		model.addAttribute("username", username);
		return "edit";
	}

}
