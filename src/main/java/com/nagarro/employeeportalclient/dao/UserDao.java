package com.nagarro.employeeportalclient.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.nagarro.employeeportalclient.model.User;

@Repository
public class UserDao {

	@Autowired
	private HibernateTemplate hibernateTemplate;

	// putting user data
	public int saveUser(User user) {
		int id = (Integer) this.hibernateTemplate.save(user);
		return id;
	}

	// getting user data
	public User getUser(String username) {
		User user = this.hibernateTemplate.get(User.class, username);
		return user;
	}


}
