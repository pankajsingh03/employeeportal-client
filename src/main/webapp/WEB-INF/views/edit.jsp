<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*"%>
<%@ page import="com.nagarro.employeeportalclient.*"%>
<%@ page import="com.nagarro.employeeportalclient.model.Employee"%>
<%@ page import="com.nagarro.employeeportalclient.controller.RestClient"%>

<%@ page isELIgnored="false"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>Edit Page</title>
<style>
* {
	font-family: "Calibri", 'Trebuchet MS', sans-serif;
}

.container {
	width: 90%;
	margin: 35px auto;
	min-width: max-content;
	border: 1px solid black;
}

#Heading {
	border: 1px solid black;
	padding: 10px;
	font-size: 28px;
	font-weight: bold;
}

p {
	color: black;
	text-align: right;
	width: 90%;
	padding: 15px 15px 15px 15px;
	margin: auto;
	font-weight: bolder;
	font-size: 25px;
}

input {
	width: 150px;
}

fieldset {
	border: 0px;
	padding: 10px 0px 10px 0px;
	width: 85%;
	margin: auto;
}

label {
	color: black;
	display: block;
	width: 380px;
	float: left;
	text-align: right;
	padding-right: 30px;
}

#error {
	color: red;
}

#submit {
	color: #fff;
	background-color: #28a745;
	border-color: #28a745;
	border-radius: 5px;
	padding: 7px;
	margin: 7px;
}

#cancel {
	color: #fff;
	background-color: #dc3545;
	border-color: #dc3545;
	border-radius: 5px;
	padding: 7px;
	margin: 7px;
	width: 150px;
}
</style>
</head>

<body>


	<%
	int k = Integer.parseInt(request.getParameter("idToUpdate"));
	Employee e = RestClient.getEmployeeByIdAPI(k);
	%>

	<header>
		<p>
			Welcome ${username} <span> <a href="login"><button>Logout!!</button></a></span>
		</p>
	</header>

	<div class="container">
		<div id="Heading">Edit Employee Details</div>
		<div id="uploader">

			<div id="inputFeild">
				<form action="updateDetails" method="post">
					<input type="text" name="username" value="${username}"
						Style="visibility: hidden;">
					<fieldset>
						<label for="employeeCode">Employee Code: </label>
						 <input type="number" name="employeeCode" id="employeeCode" min="1" value="<%=e.getEmployeeCode()%>" readonly>
					</fieldset>
					<fieldset>
						<label for="employeeName">Employee Name: </label> <input
							type="text" name="employeeName" id="employeeName" maxlength="100"
							value="<%=e.getEmployeeName()%>" required>
					</fieldset>
					<fieldset>
						<label for="location">Location : </label>
						 <input type="text" name="location" id="location" maxlength="500" Value="<%=e.getLocation()%>" required>
					</fieldset>
					<fieldset>
						<label for="email">Email</label> 
						<input type="text" name="email" id="email" Value="<%=e.getEmail()%>" maxlength="100" required>
					</fieldset>
					<fieldset>
						<label for="dateOfBirth">Date of Birth (DD/MM/YYYY) : </label> 
						<input type="text" name="dateOfBirth" id="dateOfBirth" value="<%=e.getDateOfBirth()%>" required>
					</fieldset>
					<fieldset>
						<label><input type="submit" value="Save" id="submit">
						</label> <a href="home"><button value="cancel" id="cancel">Cancel</button></a>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</body>
</html>

