<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta Http-Equiv="Cache-Control" Content="no-cache">
<meta Http-Equiv="Pragma" Content="no-cache">
<meta Http-Equiv="Expires" Content="0">
<title>Login Page</title>
<style>
		* {
			font-family: "Calibri", 'Trebuchet MS', sans-serif;
		}
		
		body, html {
			min-width: max-content;
		}
		
		#loginHeading {
			height: 25px;
			background-color: #cfd2d4;
			padding: 7px;
			font-size: 21px;
			font-weight: bold;
			/* color: #2100ff; */
			border-top: 2px solid grey;
			border-bottom: 2px solid grey;
		}
		
		#error {
			color: red;
			
		}
		
		#formDiv {
			margin: 0px;
			padding: 20px;
			background-color: #eeeeee;
		}
		
		.container {
			max-width: 500px;
			margin: 45px auto;
			border-left: 2px solid grey;
			border-right: 2px solid grey;
			min-width: 500px;
		}
		
		label {
			color: black;
			display: block;
			width: 175px;
			float: left;
		}
		
		label:after {
			content: ": "
		}
		
		fieldset {
			border: 0px;
			padding: 10px 0px 10px 0px;
			width: 450px;
			margin: auto;
		}
		
		input {
			width: 250px;
		}
		
		a {
			padding-left: 175px;
		}
		
		#submit {
			border: 1px solid black;
			width: 77px;
			float: right;
			border-radius: 6px;
		}
		
		#button {
			height: 25px;
			background-color: #cfd2d4;
			padding: 7px;
			font-size: 21px;
			font-weight: bold;
			/* color: #2100ff; */
			border-top: 2px solid grey;
			border-bottom: 2px solid grey;
		}
</style>
</head>

<body>
	<div class="login-window container">
		<div id="loginHeading">
			Login &nbsp; 
		</div>
		<form action="verifyLogin" method="post">
			<div id="formDiv">

				<fieldset>
					<label for="username" class="left">Username</label>
					 <input type="text" name="username" id="username" required>
				</fieldset>
				<fieldset>
					<label for="password" class="left">Password</label> 
					<input type="password" name="password" id="password" required>
				</fieldset>

				
			</div>
			<div id="button">
			<%
			String check=(String) request.getAttribute("errorId");
			 if(check=="1"){%>
			 <span id="error"> * username or password is incorrect</span>
			 <% }%>
				<input type="submit" id="submit" value="Login >>" />
			</div>
		</form>
	</div>
</body>
</html>