<%@page import="com.nagarro.employeeportalclient.controller.RestClient"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*"%>
<%@ page import="com.nagarro.employeeportalclient.*"%>
<%@ page import="com.nagarro.employeeportalclient.model.Employee"%>
<%@ page isELIgnored="false" %>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="ISO-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>Home Page</title>
<style>
	* {
		font-family: "Calibri", 'Trebuchet MS', sans-serif;
	  }
	
	.container {
	   width:90%;
		margin: 35px auto;
		/* background-color: burlywood; */
		min-width: max-content;
		
		border: 1px solid black;
	}
	
	#Heading {
		/* background-color: #cfd2d4; */
		border: 1px solid black;
		padding: 10px;
		font-size: 28px;
		font-weight: bold;
		text-align: center;
		height:90px;
	}
	
	p {
		color: black;
	    text-align:right;
	    width: 90%; 
	    padding: 15px 15px 15px 15px;
	    margin: auto;
	    font-weight: bolder;
	    font-size: 25px;
	}

	
	table, th, tr, td {
		border: 1px solid black;
		margin: 0px;
		padding: 0px;
		text-align: center;
	}

	#submit
	{   color: #fff;
	    background-color: #28a745;
	    border-color: #28a745;
	    border-radius:5px;
	    padding:7px;
	    margin:7px;
	}
	.headingButtons
	{	float:right;
		margin:0px 20px 0px 20px;
	}

</style>
</head>

<body>

	<header>
		<p>Welcome ${username}<span> <a href="login"><button >Logout!!</button></a></span> </p>
	</header>

  <% String checkError=(String) request.getAttribute("errorId"); %>
			
	<div class="container">
		<div id="Heading">
				<p style="text-align:left;" >Employee Listing</p>
				<button  class="headingButtons" >Download Employee Details</button>
				<button class="headingButtons" >Upload Employee Details</button>
				
		</div>
		
		<%   Employee[] result=RestClient.getAllEmployeeAPI();

		 if(result.length==0 ){%>
			<span id="error">* Sorry no data available</span>
				 <% }
			 
		if(result.length!=0 ){%>
			<div id="TableDiv">
			<table id="table" style="width: 100%;">

				<col style="width: 10%;">
				<col style="width: 20%;">
				<col style="width: 15%;">
				<col style="width: 25%;">
				<col style="width: 20%;">
				<col style="width: 10%;">
				<thead>
					<tr>
						<th>Employee Code</th>
						<th>Employee Name</th>
						<th>Location</th>
						<th>Email</th>
						<th>Date Of Birth</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					 <% 
				    for(Employee e:result)
				    {%>
	                    <tr>
	                    <td><%=e.getEmployeeCode() %></td>
	                    <td><%=e.getEmployeeName() %></td>
	                    <td><%=e.getLocation() %></td>
	                    <td><%=e.getEmail() %></td>
	                    <td><%=e.getDateOfBirth() %></td>
	                    <td><span> <a href="edit?idToUpdate=<%=e.getEmployeeCode()%>&username=${username}">
						<button >Edit</button></a></span></td>
	            	    </tr>
    				<% }%> 
				</tbody>
			</table>
		</div>		
	 <%}%>	
	</div>
</body>
</html>

